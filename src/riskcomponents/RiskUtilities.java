package riskcomponents;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * <em><h1>RiskUtilities</h1></em>
 * Contains general functions, with no specific consumer, to maintain clean code principle.
 */
public class RiskUtilities 
{
	public static final int TERRITORIES = 42;
	
	public static final int NORTHERN_AMERICA_TERRITORIES = 9;
	public static final int SOUTHERN_AMERICA_TERRITORIES = 3;
	public static final int EUROPE_TERRITORIES = 7;
	public static final int AFRICA_TERRITORIES = 6;
	public static final int ASIA_TERRITORIES = 12;
	public static final int AUSTRALIA_TERRITORIES = 5;
	
	/**
     * <em><h1>readLines</h1></em>
     * Reads a file from a given path, separating its lines to an array of strings.
     * <hr>
     * @param filename - Path to the file to be read.
     * @return An array of the lines in the file.
     * @throws IOException in case the file couldn't be found or the path is invalid.
     */
	public static String[] readLines(String filename) throws IOException 
	{
		FileReader fileReader = new FileReader(filename);

		BufferedReader bufferedReader = new BufferedReader(fileReader);
		ArrayList<String> lines = new ArrayList<String>();
		String line = null;

		while ((line = bufferedReader.readLine()) != null) 
		{
			lines.add(line);
		}

		bufferedReader.close();

		return lines.toArray(new String[lines.size()]);
	}
	
	/**
     * <em><h1>getTimestamp</h1></em>
     * Creates a timestamp of the current time. 
     * <hr>
     * @return A string containing the timestamp in HH:MM:SS format.
     */
	public static String getTimestamp()
	{
		Calendar now = Calendar.getInstance();
		
		int hour = now.get(Calendar.HOUR_OF_DAY);
		int minutes = now.get(Calendar.MINUTE);
		int seconds = now.get(Calendar.SECOND);
		
		return String.format("%02d:%02d:%02d", hour, minutes, seconds);
	}
}

package riskcomponents;

import java.util.List;

public class Country implements Cloneable
{
	private String name;
	private List<Country> borderingCountries;
	
	private boolean isOccupied;
	private Player occupant;
	
	private int forces;
	
	public Country clone() throws CloneNotSupportedException
	{
		Country newCountry = (Country) super.clone();
		
		
		/*
		newCountry.borderingCountries = new ArrayList<Country>();
		
		for(Country c : borderingCountries)
		{
			Country newBordering = new Country(name);
			newBordering.name = c.name;
			newBordering.isOccupied = c.isOccupied;
			newBordering.occupant = c.occupant;
			
			newCountry.borderingCountries.add(newBordering);
		}
		*/
		
		return newCountry;
	}
	
	public Country(String _name)
	{
		name = _name;
	}
	
	public void setBorderingCountries(List<Country> _borderingCountries)
	{
		borderingCountries = _borderingCountries;
	}
	
	public void setOccupant(Player _occupant)
	{
		isOccupied = true;
		occupant = _occupant;
	}
	
	public void setForces(int _forces)
	{
		forces = _forces;
	}
	
	public void addForces(int _forces)
	{
		forces += _forces;
	}
	
	public void subForces(int _forces)
	{
		forces -= _forces;
	}
	
	public String getName()
	{
		return name;
	}
	
	public List<Country> getBorderingCountries()
	{
		return borderingCountries;
	}
	
	public boolean isOccupied()
	{
		return isOccupied;
	}
	
	public Player getOccupant()
	{
		return occupant;
	}
	
	public int getForces()
	{
		return forces;
	}
	
	public boolean equals(Object o)
	{
		if(o == this)
			return true;
		
		if(o == null)
			return false;
		
		if(o.getClass() != this.getClass())
			return false;
		
		Country other = (Country)o;
		
		return (other.name.equals(this.name));
	}
	
	public String toString()
	{
		String s = "\n" + name + " - Occupied By " + occupant + " - " + forces + " Forces";
				
		return s;
	}
	
	public boolean isBordering(Country otherCountry)
	{
		return borderingCountries.contains(otherCountry);
	}
	
	public boolean isBordering(String otherCountryName)
	{
		return isBordering(new Country(otherCountryName));
	}
}

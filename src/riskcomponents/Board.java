package riskcomponents;

import java.util.*;

public class Board implements Cloneable 
{
	// main containers
	private Map<String, Country> countriesMap;
    private Map<String, Continent> continentsMap;
	
    // helper containers
	private List<Country> countriesList;
	private List<Country> borderingList;
	private List<Country> memberCountries;
	private List<Country> unoccupiedCountries;
    
    public Board() 
    {
    	countriesMap = new HashMap<String, Country>();
    	continentsMap = new HashMap<String, Continent>();
    	
    	countriesList = new ArrayList<Country>();
    	borderingList = new ArrayList<Country>();
    	memberCountries = new ArrayList<Country>();
    	unoccupiedCountries = new ArrayList<Country>();
    }
    
    public Board clone() throws CloneNotSupportedException
    {
    	Board newBoard = (Board) super.clone();
    	
    	newBoard.countriesMap = new HashMap<String, Country>();
    	newBoard.continentsMap = new HashMap<String, Continent>();
    	newBoard.countriesList = new ArrayList<Country>();
    	
    	// populating new board map, all initializations are based on it
    	for(Country oldCountry : countriesMap.values())
    	{
    		Country newCountry = oldCountry.clone();
    		
    		newBoard.countriesMap.put(newCountry.getName(), newCountry);
    		newBoard.countriesList.add(newCountry);
    	}
    	
    	// cloning all bordering lists in each cloned country
    	for(Country newCountry : newBoard.countriesList)
    	{
    		newBoard.borderingList = new ArrayList<Country>();
    		
    		for(Country oldBordering : newCountry.getBorderingCountries())
    		{
    			Country newBordering = newBoard.countriesMap.get(oldBordering.getName());
    			newBoard.borderingList.add(newBordering);
    		}
    		
    		newCountry.setBorderingCountries(newBoard.borderingList);
    	}
    	
    	// populating new continents map including update of each member list to the cloned countries 
    	for(Continent oldContinent : continentsMap.values())
    	{
    		Continent newContinent = oldContinent.clone();
    		
    		newBoard.continentsMap.put(newContinent.getName(), newContinent);
    		
    		newBoard.memberCountries = new ArrayList<Country>();
    		
    		for(Country oldMember : newContinent.getCountries())
    		{
    			Country newMember = newBoard.countriesMap.get(oldMember.getName());
    			newBoard.memberCountries.add(newMember);
    		}
    		
    		newContinent.setCountries(newBoard.memberCountries);
    	}
    		
    	return newBoard;
    }
    
    /**
     * <em><h1>initializeBoard</h1></em>
     * Initializes the RISK board, setting the required countries, continents and bordering countries on the board.
     * <hr>
     * @param countriesArr - Array containing the countries names (found in file countries.txt).
     * @param borderingsArr - Array containing the borders strings (found in file borderings.txt).
     * @param continentsArr - Array containing the continents strings (found in file continents.txt).
     * @return true - if the board was successfully initialized.
     */
    public boolean initializeBoard(String[] countriesArr, String[] borderingsArr, String[] continentsArr)
    {
    	int i = 0;
    	
    	Country currentCountry;
    	
    	String[] borderingSplitted;
    	
    	Continent currentContinent;
    	String[] continentSplitted;
    	String continentName;
    	int continentForces;
    	
    	System.out.println(RiskUtilities.getTimestamp() + " >> INITIALIZING BOARD\n");
    	
    	//setting countriesMap and countriesList
    	//each countryName is represented as: countryName
    	System.out.println("Setting up countriesMap and countriesList...");
    	
    	for(String countryName : countriesArr)
    	{
    		currentCountry = new Country(countryName);
    		
    		countriesMap.put(countryName, currentCountry);
    		countriesList.add(currentCountry);
    	}
    	
    	System.out.println("countriesMap and countriesList setup done!\n");
    	
    	//setting bordering countries for each country in countriesMap
    	//each borderingStr is represented as: countryName,countryName,...,countryName
    	System.out.println("Setting up bordering countries for each country in countriesMap...");
    	
    	for(String borderingStr : borderingsArr)
    	{
    		borderingSplitted = borderingStr.split(",");
    		
    		borderingList = new ArrayList<Country>();
    		
    		//building bordering list for the country in index 0
    		for(i = 1; i < borderingSplitted.length; i++)
    		{
    			currentCountry = countriesMap.get(borderingSplitted[i]);
    			borderingList.add(currentCountry);
    		}
    		
    		currentCountry = countriesMap.get(borderingSplitted[0]);
    		currentCountry.setBorderingCountries(borderingList);
    	}
    	
    	System.out.println("Bordering countries setup done!\n");
    	
    	//setting continentsMap
    	//each continentStr is represented as: continentName,numForces,{borderingCountries}
    	System.out.println("Setting up continentsMap...");
    	
    	for(String continentStr : continentsArr)
    	{
    		memberCountries = new ArrayList<Country>();
    		continentSplitted = continentStr.split(",");
    		
    		continentName = continentSplitted[0];
    		continentForces = Integer.parseInt(continentSplitted[1]);
    		
    		for(i = 2; i < continentSplitted.length; i++)
    		{
    			currentCountry = countriesMap.get(continentSplitted[i]);
    			memberCountries.add(currentCountry);
    		}
    		
    		currentContinent = new Continent(continentName, continentForces, memberCountries);
    		continentsMap.put(continentName, currentContinent);
    	}
    	
    	System.out.println("continentsMap setup done!\n");
    	System.out.println(RiskUtilities.getTimestamp() + " >> BOARD INITIALIZATION COMPLETE");
    	
    	return true;
    }
    
    /**
     * Lists all currently unoccupied countries on the board.
     * @return ArrayList containing unoccupied countries.
     */
    public List<Country> getUnoccupiedCountries()
    {
    	unoccupiedCountries = new ArrayList<Country>();
    	
    	for(Country currentCountry : countriesList)
    	{
    		if(!currentCountry.isOccupied())
    		{
    			unoccupiedCountries.add(currentCountry);
    		}
    	}
    	
    	return unoccupiedCountries;
    }
    
    /**
     * <em><h1>getCountriesList</h1></em>
     * Getter for the list of countries.
     * <hr>
     * @return An ArrayList containing all countries.
     */
    public List<Country> getCountriesList()
    {
    	return countriesList;
    }
    
    public List<Continent> getContinentsList()
    {
    	return new ArrayList<Continent>(continentsMap.values());
    }
    
    public Map<String, Continent> getContinents()
    {
    	return continentsMap;
    }
    
    /**
     * <em><h1>getCountryByName</h1></em>
     * Getter for a Country object of a given country name.
     * <hr>
     * @param countryName - The name of the country to be returned.
     * @return The Country element associated with the given country name.
     */
    public Country getCountryByName(String countryName)
    {
    	return countriesMap.get(countryName);
    }
    
    /**
     * <em><h1>checkBordering</h1></em>
     * Checks whether two given countries are bordering or not.
     * <hr>
     * @param countryAname - First country name.
     * @param countryBname - Second country name.
     * @return true - if the countries are bordering.<br>false - if they are not bordering.
     */
    public boolean checkBordering(String countryAname, String countryBname)
    {
    	Country countryA = countriesMap.get(countryAname);
    	Country countryB = countriesMap.get(countryBname);
    	
    	return ( countryA.isBordering(countryB) && countryB.isBordering(countryA) );
    }
}

package riskcomponents;

import java.util.List;

public class Continent implements Cloneable 
{
	private String name;
	private int bonusForces;
	private List<Country> countries;
	
	public Continent(String _name, int _bonusForces, List<Country> _countries)
	{
		name = _name;
		bonusForces = _bonusForces;
		countries = _countries;
	}
	
	public Continent() {
		// TODO Auto-generated constructor stub
	}

	public Continent clone() throws CloneNotSupportedException
	{
		Continent newContinent = (Continent) super.clone();

		return newContinent;
	}
	
	public String getName()
	{
		return name;
	}
	
	public int getBonusForces()
	{
		return bonusForces;
	}
	
	public List<Country> getCountries()
	{
		return countries;
	}
	
	public void setCountries(List<Country> countries)
	{
		this.countries = countries;
	}
	
	public boolean equals(Object o)
	{
		if(o == this)
			return true;
		
		if(o == null)
			return false;
		
		if(o.getClass() != this.getClass())
			return false;
		
		Continent other = (Continent)o;
		
		return (other.name.equals(this.name));
	}
}

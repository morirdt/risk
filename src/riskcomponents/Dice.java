package riskcomponents;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Dice 
{
	private Random rng;
	
	private ArrayList<Integer> diceList;
	private int[] diceArr;
	
	public Dice() 
	{
		rng = new Random();
	}
	
	public int[] roll(int diceAmount)
	{
		diceList = new ArrayList<Integer>();
		
		for (int i = 0; i < diceAmount; i++) 
		{
			diceList.add(rng.nextInt(6) + 1);
		}
		
		//sorting playerDice highest to lowest
		Collections.sort(diceList);
		Collections.reverse(diceList);
		
		diceArr = new int[diceList.size()];
		
		for(int i = 0; i < diceArr.length; i++)
		{
			diceArr[i] = diceList.get(i);
		}
		
		return diceArr;
	}
}

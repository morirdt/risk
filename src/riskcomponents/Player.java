package riskcomponents;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Player 
{
	protected String name;
	protected Color color;
	protected int forces;
	protected int turnInCounter;
	
	protected Map<String, Country> occupiedCountries;
	protected Map<String, Continent> occupiedContinents;
	
	public Player(String _name, int _forces, Color _color)
	{
		name = _name;
		forces = _forces;
		color = _color;
		
		occupiedCountries = new HashMap<String, Country>();
		occupiedContinents = new HashMap<String, Continent>();
	}
	
	public String getName()
	{
		return name;
	}
	
	public int getForces()
	{
		return forces;
	}
	
	
	public Color getColor()
	{
		return color;
	}
	
	public List<Country> getOccupiedCountries()
	{
		return new ArrayList<Country>(occupiedCountries.values());
	}
	
	public List<Continent> getOccupiedContinents()
	{
		return new ArrayList<Continent>(occupiedContinents.values());
	}
	
	public void setForces(int _forces)
	{
		forces = _forces;
	}
	
	public void addForces(int _forces)
	{
		forces += _forces;
	}
	
	public void subForces(int _forces)
	{
		forces -= _forces;
	}
	
	public void addCountry(Country _country)
	{
		occupiedCountries.put(_country.getName(), _country);
	}
	
	public void removeCountry(String _countryName)
	{
		occupiedCountries.remove(_countryName);
	}
	
	public void removeCountry(Country _country)
	{
		occupiedCountries.remove(_country.getName());
	}
	
	public void addContinent(Continent _continent)
	{
		occupiedContinents.put(_continent.getName(), _continent);
	}
	
	public void removeContinent(String _continentName)
	{
		occupiedContinents.remove(_continentName);
	}
	
	public boolean equals(Object o)
	{
		if(o == this)
			return true;
		
		if(o == null)
			return false;
		
		if(o.getClass() != this.getClass())
			return false;
		
		Player otherPlayer = (Player)o;
		return (otherPlayer.name.equals(this.name));
	}
	
	public String toString()
	{
		return name;
	}
}

package mvc;

import java.util.*;
import java.awt.Color;
import java.io.IOException;

import javax.swing.JOptionPane;

import botcomponents.*;
import enums.*;
import riskcomponents.*;

public class RiskModel extends Observable 
{
	private static RiskModel instance;
	
	private static final String COUNTRIES_FILENAME = "resources\\countries.txt";
	private static final String CONTINENTS_FILENAME = "resources\\continents.txt";
	private static final String BORDERING_FILENAME = "resources\\borderings.txt";
	
	private static final RiskPhase DEPLOY = RiskPhase.DEPLOY;
	private static final RiskPhase ATTACK = RiskPhase.ATTACK;
	private static final RiskPhase FORTIFY = RiskPhase.FORTIFY;
	
	public static final int REINFORCE_TERRITORY = 0;
	public static final int OCCUPY_TERRITORY = 1;
	
	public static final int COUNTRIES_FOR_ATTACK = 2;
	public static final int COUNTRIES_FOR_FORTIFY = 2;
	
	private RiskPhase gamePhase;
	
	private int i;
	
	private ArrayList<Player> players;
	private int numPlayers;
	
	private String[] countriesArr;
	private String[] borderingsArr;
	private String[] continentsArr;
	
	private ArrayList<Integer> optionsList;
	private Integer[] attackDiceOptions;
	
	private int[] attackerRolls;
	private int[] defenderRolls;
	private int attackerDiceChoice;
	private int defenderDiceChoice;
	
	private int attackerLosses;
	private int defenderLosses;
	
	private Board board;
	private Dice dice;
	
	private Player currentPlayer;
	private int currentPlayerIndex;
	
	private RiskModel() {}
	
	public static synchronized RiskModel getInstance()
	{
		if(instance == null)
			instance = new RiskModel();
		
		return instance;
	}

	/**
     * <em><h1>initializeModel</h1></em>
     * Initializes the RISK model and all the needed assets for the game to operate properly.
     * <hr>
     * @param playerNames - List containing the name of the players.
     * @param gamemode - The mode, requested by the user. Located in {@link RiskGamemode}
     * @return True - if the model was successfully initialized.
     **/
	public boolean initializeModel(List<String> playerNames, RiskGamemode gamemode)
	{
		System.out.println(RiskUtilities.getTimestamp() + " >> INITIALIZING MODEL\n");
		
		try 
		{
			countriesArr = RiskUtilities.readLines(COUNTRIES_FILENAME);
			borderingsArr = RiskUtilities.readLines(BORDERING_FILENAME);
			continentsArr = RiskUtilities.readLines(CONTINENTS_FILENAME);
		}
		
		catch(IOException ioe)
		{
			ioe.printStackTrace();
			quitGame();
		}
		
		//initializing board
		board = new Board();
		board.initializeBoard(countriesArr, borderingsArr, continentsArr);
		
		//initializing randomizer
		dice = new Dice();
		
		//initializing players
		players = new ArrayList<Player>();
		numPlayers = playerNames.size();
		currentPlayerIndex = -1;
		
		int currentForces = calculateForcesForPlayer();
		Color[] playerColors = { Color.BLUE, Color.RED };
		
		if(gamemode == RiskGamemode.HUMAN_VS_HUMAN)
		{
			for(String currentName : playerNames)
			{
				Player newPlayer = new Player(currentName, currentForces, playerColors[players.size()]);
				players.add(newPlayer);
			}
		}
		
		else if(gamemode == RiskGamemode.HUMAN_VS_COMPUTER)
		{
			players.add(new Player(playerNames.get(0), currentForces, playerColors[0]));
			players.add(new Bot(playerNames.get(1), currentForces, playerColors[1]));
		}
		
		//initializing game phase
		gamePhase = DEPLOY;
		
		System.out.println("\n" + RiskUtilities.getTimestamp() + " >> MODEL INITIALIZATION COMPLETE");
		
		return true;
	}
	
	public Board getBoard()
	{
		return board;
	}
	
	public RiskPhase getGamePhase()
	{
		return gamePhase;
	}
	
	public List<Player> getPlayers()
	{
		return players;
	}
	
	public Player getCurrentPlayer()
	{
		return currentPlayer;
	}
	
	public void startGame()
	{
		System.out.println("\n" + RiskUtilities.getTimestamp() + " >> STARTING GAME");
		nextPlayer();
	}
	
	public void quitGame()
	{
		System.out.println("\n" + RiskUtilities.getTimestamp() + " >> GAME OVER");
		System.exit(0);
	}
	
	/**
     * <em><h1>nextPlayer</h1></em>
     * Handles the turn switching, updates the current player that supposed to make a move.
     * <hr>
     **/
	public void nextPlayer()
	{
		if(gamePhase == DEPLOY)
		{
			currentPlayerIndex++;
			currentPlayerIndex = (currentPlayerIndex >= players.size()) ? 0 : currentPlayerIndex;
			currentPlayer = players.get(currentPlayerIndex);
			
			// checking if both players have no forces left to deploy
			if(players.get(0).getForces() == 0 && players.get(1).getForces() == 0)
			{
				gamePhase = ATTACK;
			}
			
			// checking if current player has no forces left
			else if(currentPlayer.getForces() == 0)
			{
				nextPlayer();
			}
			
			else
			{
				System.out.println("\n� � � " + currentPlayer.getName() + "'s TURN � � �");
			}
		}
		
		else if(gamePhase == ATTACK)
		{
			System.out.println("\n� � � " + currentPlayer.getName() + "'s TURN � � �");
		}
		
		else if(gamePhase == FORTIFY)
		{
			gamePhase = DEPLOY;
			nextPlayer();
		}
		
		if(currentPlayer instanceof Bot)
		{
			//System.out.println("Bot");
			((Bot) currentPlayer).executeMove();
		}
	}
	
	public void incrementPhase()
	{
		gamePhase = FORTIFY;
		
		if(currentPlayer instanceof Bot)
		{
			((Bot)currentPlayer).executeMove();
		}
	}
	
	/**
     * <em><h1>reinforce</h1></em>
     * Reinforces a chosen country with an amount of forces to deploy.
     * <hr>
     * @param toReinforceName - Name of a country to reinforce.
     * @param forcesToAdd - Number of forces to deploy.
     * @return One of the codes located in {@link ReinforceCode}, based on the reinforcement result.<br>
     * <b><code>COUNTRY_NOT_FOUND</code></b> in case country doesn't exist.<br>
     * <b><code>INVALID_COUNTRY</code></b> in case country is not occupied by the current player.<br>
     * <b><code>NOT_ENOUGH_FORCES</code></b> in case player has less forces than wanted to reinforce.<br>
     * <b><code>SUCCESS</code></b> in case reinforcement was successfully executed.<br>
     **/
	public ReinforceCode reinforce(String toReinforceName, int forcesToAdd)
	{
		Country toReinforce = board.getCountryByName(toReinforceName);
		
		// country not found (should not happen when handling GUI)
		if(toReinforce == null) 
		{
			System.out.println("[ERROR] Country not found!");
			return ReinforceCode.COUNTRY_NOT_FOUND;
		}
		
		// current player doesn't own the country
		if(toReinforce.isOccupied() && !toReinforce.getOccupant().equals(currentPlayer)) 
		{
			System.out.println("[ERROR] You don't own this country!");
			return ReinforceCode.INVALID_COUNTRY;
		}
		
		// calling "reinforce" method with forces value of zero means asking the user for input
		if(forcesToAdd <= REINFORCE_TERRITORY) 
		{
			try
			{
				forcesToAdd = Integer.parseInt(JOptionPane.showInputDialog(null, 
						"How many forces would you like to reinforce " + toReinforceName + " with? (" + currentPlayer.getForces() + " remaining)", 
						"Forces Input", 
						JOptionPane.QUESTION_MESSAGE));
			}
			catch(NumberFormatException nfe)
			{
				System.out.println("[MESSAGE] True commanders don't despise war. Please take this seriously.");
				return ReinforceCode.REINFORCE_FAIL;
			}
		}
		
		// player doesn't have enough forces
		if(currentPlayer.getForces() < forcesToAdd) 
		{
			System.out.println("[ERROR] You don't have enough forces!");
			return ReinforceCode.NOT_ENOUGH_FORCES;
		}
		
		toReinforce.addForces(forcesToAdd);
		toReinforce.setOccupant(currentPlayer);
		currentPlayer.subForces(forcesToAdd);
		currentPlayer.addCountry(toReinforce);
		
		System.out.println("[REINFORCEMENT] Reinforced " + toReinforceName + " with " + forcesToAdd + " forces. " + 
							"They now have " + currentPlayer.getForces() + " forces remaining.");
		
		checkContinentOccupation();
		nextPlayer();
		
		BoardView.getInstance().updateLabelValues();
		
		return ReinforceCode.REINFORCE_SUCCESS;
	}
	
	/**
     * <em><h1>attack</h1></em>
     * Performs an attack from a chosen country towards a chosen opponent's country.
     * <hr>
     * @param attackerCountryName - Name of the country to perform the attack from.
     * @param defenderCountryName - Name of the country to attack.
     * @return One of the codes located in {@link AttackCode}, based on the reinforcement result.<br>
     * <b><code>COUNTRY_NOT_FOUND</code></b> in case one or both of the countries don't exist.<br>
     * <b><code>INVALID_COUNTRY</code></b> in case attacker country is not occupied by the current player.<br>
     * <b><code>SAME_OCCUPANT </code></b> in case both countries are occupied by the same player.<br>
     * <b><code>COUNTRIES_NOT_BORDERING</code></b> in case countries are not bordering.<br>
     * <b><code>NOT_ENOUGH_FORCES</code></b> in case player has only 1 force in the country he chose to attack from.<br>
     * <b><code>FAIL</code></b> in case attack has failed due to illegal input from the attacker or defender.<br>
     * <b><code>SUCCESS</code></b> in case attack was successfully executed.<br>
     **/
	public AttackCode attack(String attackerCountryName, String defenderCountryName)
	{
		Country attackerCountry = board.getCountryByName(attackerCountryName);
		Country defenderCountry = board.getCountryByName(defenderCountryName);
		
		// Country doesn't exist in board
		if(attackerCountry == null || attackerCountry == null)
		{
			System.out.println("[ERROR] Country not found!");
			return AttackCode.COUNTRY_NOT_FOUND;
		}
		
		// Current player doesn't own the attacking country
		if(attackerCountry.isOccupied() && !attackerCountry.getOccupant().equals(currentPlayer))
		{
			System.out.println("[ERROR] You don't own the country you're attacking from!");
			return AttackCode.INVALID_COUNTRY;
		}
		
		// Current player own the defending country
		if(defenderCountry.isOccupied() && defenderCountry.getOccupant().equals(currentPlayer))
		{
			System.out.println("[ERROR] You can't attack your own country!");
			return AttackCode.SAME_OCCUPANT;
		}
		
		// The two countries are not bordering
		if(board.checkBordering(attackerCountryName, defenderCountryName) == false)
		{
			System.out.println("[ERROR] The countries you chose are not bordering!");
			return AttackCode.COUNTRIES_NOT_BORDERING;
		}
		
		// Player has only 1 force in attacking country
		if(attackerCountry.getForces() == 1)
		{
			System.out.println("[ERROR] You can't attack from a country with 1 force!");
			return AttackCode.NOT_ENOUGH_FORCES;
		}
		
		Player attacker = attackerCountry.getOccupant();
		Player defender = defenderCountry.getOccupant();
		
		System.out.println("[ATTACK] " + attacker + " is attacking " + defenderCountryName + "!");
		
		initAttackComponents();
		
		initAttackOptions(attackerCountry.getForces(), AttackRole.ATTACKER);
		
		if(attacker instanceof Bot)
		{
			attackerDiceChoice = ((Bot)attacker).getBotDices(optionsList);
		}
		
		else
		{
			try
			{
				attackerDiceChoice = getDiceChoice(attacker.getName(), defenderCountry, AttackRole.ATTACKER);
			}
			
			catch(NullPointerException npe)
			{
				System.out.println("\n" + attacker.getName() + " hasn't chosen a number of dices to roll!");
				return AttackCode.ATTACK_FAIL;
			}
		}
		
		initAttackOptions(defenderCountry.getForces(), AttackRole.DEFENDER);
		
		if(defender instanceof Bot)
		{
			defenderDiceChoice = ((Bot)defender).getBotDices(optionsList);
		}
		
		else
		{
			try 
			{
				defenderDiceChoice = getDiceChoice(defender.getName(), defenderCountry, AttackRole.DEFENDER);
			} 
			
			catch (NullPointerException npe) 
			{
				System.out.println("\n" + defender.getName() + " hasn't chosen a number of dices to roll!");
				return AttackCode.ATTACK_FAIL;
			}
		}
		
		// at this point: both countries are valid, 
		// attacker has chosen a legal amount of dices
		// defender has chosen a legal amount of dices
		// attack should be successfully executed
		
		attackerRolls = dice.roll(attackerDiceChoice);
		defenderRolls = dice.roll(defenderDiceChoice);
		
		// highest pair is located at index 0
		if (attackerRolls[0] > defenderRolls[0]) 
			defenderLosses++;
		
		else if (attackerRolls[0] < defenderRolls[0])
			attackerLosses++;
		
		// second highest pair is located at index 1
		if (attackerDiceChoice > 1 && defenderDiceChoice > 1) 
		{
			if (attackerRolls[1] > defenderRolls[1]) 
				defenderLosses++;
			
			else if (attackerRolls[1] < defenderRolls[1]) 
				attackerLosses++;
		}
		
		attackerCountry.subForces(attackerLosses);
		defenderCountry.subForces(defenderLosses);

		
		// checking if defender has lost the attacked territory
		if(defenderCountry.getForces() < 1)
		{
			System.out.println("[ATTACK REPORT] " + currentPlayer + " has occupied " + defenderCountryName + "!");
			
			defender.removeCountry(defenderCountry);
			attacker.addCountry(defenderCountry);
			
			defenderCountry.setOccupant(attacker);
			
			// checking if the defeated defender has no territories left
			if(defender.getOccupiedCountries().isEmpty()) 
			{
				JOptionPane.showMessageDialog(null, attacker + " has won the war because " + defender + " had lost all of their territories!", "Game Over", JOptionPane.INFORMATION_MESSAGE);
				quitGame();
			}
			
			attackerCountry.subForces(1);
			defenderCountry.setForces(1);
			
			checkContinentOccupation();
		}
		
		else if(attackerCountry.getForces() < 1)
		{
			attacker.removeCountry(defenderCountry);
			defender.addCountry(defenderCountry);
			
			attackerCountry.setOccupant(defender);
			
			if(attacker.getOccupiedCountries().isEmpty()) 
			{
				JOptionPane.showMessageDialog(null, defender + " has won the war because " + attacker + " had lost all of their territories!", "Game Over", JOptionPane.INFORMATION_MESSAGE);
				quitGame();
			}
			
			defenderCountry.subForces(1);
			attackerCountry.setForces(1);
			
			checkContinentOccupation();
		}
		
		else
		{
			System.out.println("[ATTACK REPORT] " + attackerCountryName + " now has " + attackerCountry.getForces() + " forces.");
			System.out.println("[ATTACK REPORT] " + defenderCountryName + " now has " + defenderCountry.getForces() + " forces.");
		}
		
		BoardView.getInstance().updateLabelValues();
		
		return AttackCode.ATTACK_SUCCESS;
	}
	
	/**
     * <em><h1>fortify</h1></em>
     * Allows the player to transfer forces from a territory to another, only if they are bordering.
     * <hr>
     * @param sourceCountryName - Name of a country to transfer forces from.
     * @param destinationCountryName - Name of a country to transfer forces to.
     * @return One of the codes located in {@link FortifyCode}, based on the fortification result.<br>
     * <b><code>COUNTRY_NOT_FOUND</code></b> in case one or both of the countries don't exist.<br>
     * <b><code>INVALID_COUNTRY</code></b> in case one or both of the countries is not occupied by the current player.<br>
     * <b><code>COUNTRIES_NOT_BORDERING</code></b> in case countries are not bordering.<br>
     * <b><code>NOT_ENOUGH_FORCES</code></b> in case player has less forces in the territory than they wanted to transfer.<br>
     * <b><code>SUCCESS</code></b> in case fortification was successfully executed.<br>
     **/
	public FortifyCode fortify(String sourceCountryName, String destinationCountryName)
	{
		Country sourceCountry = board.getCountryByName(sourceCountryName);
		Country destinationCountry = board.getCountryByName(destinationCountryName);
		int forcesToTransfer = 0;
		
		// Country doesn't exist in board
		if(sourceCountry == null || destinationCountry == null)
		{
			System.out.println("[ERROR] Country not found!");
			return FortifyCode.COUNTRY_NOT_FOUND;
		}
		
		// Current player doesn't own at least one of the countries he chose
		if(!sourceCountry.getOccupant().equals(currentPlayer) || 
				!destinationCountry.getOccupant().equals(currentPlayer))
		{
			System.out.println("[ERROR] You don't own one or both of those countries!");
			return FortifyCode.INVALID_COUNTRY;
		}
		
		// The two countries are not bordering
		if(!board.checkBordering(sourceCountryName, destinationCountryName))
		{
			System.out.println("[ERROR] The countries you chose are not bordering!");
			return FortifyCode.COUNTRIES_NOT_BORDERING;
		}
		
		if(sourceCountry.getForces() == 1)
		{
			System.out.println("[ERROR] You can't transfer forces from a country with 1 force!");
			return FortifyCode.NOT_ENOUGH_FORCES;
		}
		
		if(currentPlayer instanceof Bot)
		{
			forcesToTransfer = ((Bot)currentPlayer).getBotFortification(sourceCountry.getForces());
		}
		
		else
		{
			try
			{
				forcesToTransfer = Integer.parseInt(JOptionPane.showInputDialog(null, 
						"How many forces would you like to transfer from " + sourceCountryName + " to " + destinationCountryName + "?", 
						"Forces Input", 
						JOptionPane.QUESTION_MESSAGE));
			}
		
			catch(NumberFormatException nfe)
			{
				System.out.println("[MESSAGE] True commanders don't despise war. Please take this seriously.");
				return FortifyCode.FORTIFY_FAIL;
			}
		}
		
		// Player has less forces in source country than he chose to transfer 
		if(forcesToTransfer >= sourceCountry.getForces())
		{
			System.out.println("[ERROR] You are trying to fortify more forces than you have in " + sourceCountryName + "!");
			return FortifyCode.NOT_ENOUGH_FORCES;
		}
		
		// transferring the forces between the two countries by removing from source and adding to destination
		sourceCountry.subForces(forcesToTransfer);
		destinationCountry.addForces(forcesToTransfer);
		
		System.out.println("[TRANSFER] " + currentPlayer  + " has successfully transferred " + forcesToTransfer + " forces from " + sourceCountryName + " to " + destinationCountryName);
		
		BoardView.getInstance().updateLabelValues();
		
		return FortifyCode.SUCCESS;
	}
	
	public Board simulate(RiskMove move)
	{
		Board newBoard = null;
		
		try 
		{
			newBoard = board.clone();
		}
		
		catch (CloneNotSupportedException cnse) 
		{
			cnse.printStackTrace();
		}
		
		if(move instanceof ReinforcementMove)
		{
			simulateReinforcement((ReinforcementMove)move, newBoard);
		}
		
		else if(move instanceof AttackMove)
		{
			simulateAttack((AttackMove)move, newBoard);
		}
		
		else if(move instanceof FortificationMove)
		{
			simulateFortification((FortificationMove)move, newBoard);
		}
		
		return newBoard;
	}
	
	private void simulateReinforcement(ReinforcementMove move, Board newBoard)
	{
		Country target = newBoard.getCountryByName(move.getTarget().getName());
		target.addForces(move.getForces());
		target.setOccupant(currentPlayer);
	}
	
	private void simulateAttack(AttackMove move, Board newBoard)
	{
		Country attackerCountry = newBoard.getCountryByName(move.getAttackerCountry().getName());
		Country defenderCountry = newBoard.getCountryByName(move.getDefenderCountry().getName());
		
		initAttackComponents();
		
		initAttackOptions(attackerCountry.getForces(), AttackRole.ATTACKER);
		int attackerDices = attackDiceOptions[attackDiceOptions.length - 1];
		
		initAttackOptions(defenderCountry.getForces(), AttackRole.DEFENDER);
		int defenderDices = attackDiceOptions[attackDiceOptions.length - 1];
		
		attackerRolls = dice.roll(attackerDices);
		defenderRolls = dice.roll(defenderDices);
		
		// highest pair is located at index 0
		if (attackerRolls[0] > defenderRolls[0]) 
			defenderLosses++;
		
		else if (attackerRolls[0] < defenderRolls[0])
			attackerLosses++;
		
		// second highest pair is located at index 1
		if (attackerDiceChoice > 1 && defenderDiceChoice > 1) 
		{
			if (attackerRolls[1] > defenderRolls[1]) 
				defenderLosses++;
			
			else if (attackerRolls[1] < defenderRolls[1]) 
				attackerLosses++;
		}
		
		attackerCountry.subForces(attackerLosses);
		defenderCountry.subForces(defenderLosses);
		
		if(defenderCountry.getForces() < 1)
		{
			defenderCountry.setOccupant(attackerCountry.getOccupant());
			
			attackerCountry.subForces(1);
			defenderCountry.setForces(1);
		}
		
		if(attackerCountry.getForces() < 1)
		{
			attackerCountry.setOccupant(defenderCountry.getOccupant());
			
			defenderCountry.subForces(1);
			attackerCountry.setForces(1);
		}
	}
	
	private void simulateFortification(FortificationMove move, Board newBoard)
	{
		Country sourceCountry = newBoard.getCountryByName(move.getSourceCountry().getName());
		Country destinationCountry = newBoard.getCountryByName(move.getDestinationCountry().getName());
		
		int forcesToTransfer = 0;
		
		if(currentPlayer instanceof Bot)
		{
			forcesToTransfer = ((Bot)currentPlayer).getBotFortification(sourceCountry.getForces());
		}
		
		sourceCountry.subForces(forcesToTransfer);
		destinationCountry.addForces(forcesToTransfer);
	}
	
	/* Private Methods */ 
	
	private int calculateForcesForPlayer()
	{
		return ( 50 - (numPlayers * 5) );
	}
	
	private void initAttackComponents()
	{
		attackerDiceChoice = 1;
		defenderDiceChoice = 1;
		attackerLosses = 0;
		defenderLosses = 0;
	}
	
	private void initAttackOptions(int countryForces, AttackRole role)
	{
		optionsList = new ArrayList<Integer>();
		
		if(role == AttackRole.ATTACKER)
		{
			if(countryForces > 1)
				optionsList.add(1);
			
			if(countryForces > 2)
				optionsList.add(2);
			
			if(countryForces > 3)
				optionsList.add(3);
		}
		
		else if(role == AttackRole.DEFENDER)
		{
			if(countryForces >= 1)
				optionsList.add(1);
			
			if(countryForces >= 2)
				optionsList.add(2);
		}
		
		attackDiceOptions = new Integer[optionsList.size()];
		
		for(i = 0; i < attackDiceOptions.length; i++)
		{
			attackDiceOptions[i] = optionsList.get(i);
		}
	}
	
	private int getDiceChoice(String playerName, Country country, AttackRole role)
	{
		String playerAction = (role == AttackRole.ATTACKER) ? "attacking" : "defending";
		String countryName = country.getName();
		
		return (int) (JOptionPane.showInputDialog(null, 
				playerName + ", you are " + playerAction + " " + countryName + ". how many dices will you roll?", 
				"Dices Input", 
				JOptionPane.QUESTION_MESSAGE, 
				null, 
				attackDiceOptions, 
				attackDiceOptions[0]));
	}
	
	private void checkContinentOccupation()
	{
		List<Continent> allContinents = board.getContinentsList();
		List<Continent> playerContinents = currentPlayer.getOccupiedContinents();
		List<Country> playerCountries = currentPlayer.getOccupiedCountries();
		
		for(Continent continent : allContinents)
		{
			if(!playerContinents.contains(continent) && playerCountries.containsAll(continent.getCountries()))
			{
				System.out.println("[MESSAGE] " + currentPlayer + " has occupied " + continent.getName() + " and gained " + continent.getBonusForces() + " forces!");
				currentPlayer.addForces(continent.getBonusForces());
				currentPlayer.addContinent(continent);
			}
		}
	}
}

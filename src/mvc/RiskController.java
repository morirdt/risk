package mvc;

import java.util.*;
import java.awt.event.*;
import javax.swing.*;

import riskcomponents.*;
import enums.*;


public class RiskController implements ActionListener
{	
	private RiskModel model;
	
	private RiskView currentView;
	private GamemodeView nextView;
	
	public RiskController(RiskModel model, RiskView currentView) 
	{
		this.model = model;
		
		this.currentView = currentView;
		this.currentView.addActionListeners(this);
		
		this.nextView = new GamemodeView(currentView);
		this.nextView.addActionListeners(new GamemodeController(model, nextView, currentView));
	}

	public void actionPerformed(ActionEvent evt) 
	{
		String actionCommand = evt.getActionCommand();
		
		if(actionCommand.equals(RiskView.NEW_GAME_ACTION))
		{
			currentView.hideFrame();
			nextView.showFrame();
		}
	}	
}

class GamemodeController implements ActionListener
{
	private RiskModel model;
	
	private RiskView previousView;
	private GamemodeView currentView;
	private PlayerNamesView nextView;
	
	public GamemodeController(RiskModel model, GamemodeView currentView, RiskView previousView)
	{
		this.model = model;
		
		this.previousView = previousView;
		this.currentView = currentView;
		
		this.nextView = new PlayerNamesView();
		this.nextView.addActionListeners(new PlayerNamesController(model, nextView, currentView));
	}
	
	public void actionPerformed(ActionEvent evt) 
	{
		String actionCommand = evt.getActionCommand();
		
		if(actionCommand.equals(GamemodeView.HVH_ACTION))
		{
			currentView.hideFrame();
			nextView.showFrame();
		}
		
		if(actionCommand.equals(GamemodeView.HVC_ACTION))
		{
			//JOptionPane.showMessageDialog(currentView, "Feature not supported!", "RISK", JOptionPane.WARNING_MESSAGE);
			model.initializeModel(Arrays.asList(new String[] { "Human", "Bot" }), RiskGamemode.HUMAN_VS_COMPUTER);
			model.startGame();
			
			JOptionPane.showMessageDialog(currentView, "Game has started!", "Success", JOptionPane.INFORMATION_MESSAGE);
			
			BoardView boardView = BoardView.getInstance();
			currentView.hideFrame();
			boardView.addActionListeners(new BoardController(model, boardView));
			boardView.addWindowListener(new BoardWindowController(model));
			boardView.showFrame();
			boardView.fullscreenFrame();
		}
		
		if(actionCommand.equals(GamemodeView.BACK_ACTION))
		{
			currentView.hideFrame();
			previousView.showFrame();
		}
	}
}

class PlayerNamesController implements ActionListener
{
	private RiskModel model;
	
	private GamemodeView previousView;
	private PlayerNamesView currentView;
	private BoardView nextView;
	
	public PlayerNamesController(RiskModel model, PlayerNamesView currentView, GamemodeView previousView)
	{
		this.model = model;
		
		this.previousView = previousView;
		this.currentView = currentView;
	}
	
	public void actionPerformed(ActionEvent evt) 
	{
		String actionCommand = evt.getActionCommand();
		
		if(actionCommand.equals(PlayerNamesView.START_GAME_ACTION))
		{
			List<String> playerNames = currentView.getPlayerNames();
			
			if(playerNames.get(0).equals(playerNames.get(1)))
			{
	            JOptionPane.showMessageDialog(currentView, "You can't have the same name for both players!", "Error", JOptionPane.ERROR_MESSAGE);
			}
			
			else
			{
				JOptionPane.showMessageDialog(currentView, "Game has started!", "Success", JOptionPane.INFORMATION_MESSAGE);
				
				model.initializeModel(playerNames, RiskGamemode.HUMAN_VS_HUMAN);
				model.startGame();
				
				nextView = BoardView.getInstance();
				
				currentView.hideFrame();
				
				nextView.showFrame();
				nextView.fullscreenFrame();
				nextView.addActionListeners(new BoardController(model, nextView));
				nextView.addWindowListener(new BoardWindowController(model));
			}
		}
		
		else if(actionCommand.equals(PlayerNamesView.BACK_ACTION))
		{
			currentView.hideFrame();
			previousView.showFrame();
		}
	}
}

class BoardController implements ActionListener
{
	private RiskModel model;
	private BoardView currentView;
	
	private List<Country> unoccupiedCountries;
	private List<String> attackCountries;
	private List<String> fortifyCountries;
	
	private Country currentCountry;
	
	public BoardController(RiskModel model, BoardView currentView)
	{
		this.model = model;
		this.currentView = currentView;
		
		attackCountries = new ArrayList<String>();
		fortifyCountries = new ArrayList<String>();
	}
	
	/**
	 * <em><h1>actionPerformed</h1></em>
     * Detects actions taken over the board frame, and handling them according to the action command.
     * <hr>
	 * @param evt - Event that was taken on the board frame.
	 */
	public void actionPerformed(ActionEvent evt) 
	{
		String actionCommand = evt.getActionCommand();
		
		RiskPhase gamePhase = model.getGamePhase();
		currentCountry = model.getBoard().getCountryByName(actionCommand);
		
		if(actionCommand.equals(BoardView.END_ATTACK_COMMAND))
		{
			if(gamePhase == RiskPhase.ATTACK)
			{
				model.incrementPhase();
				currentView.updateLabelValues();
			}
			
			else
			{
				System.out.println("[ERROR] You are not attacking right now!");
			}
			
			return;
		}
		
		if(actionCommand.equals(BoardView.END_FORTIFY_COMMAND))
		{
			if(gamePhase == RiskPhase.FORTIFY)
			{
				model.nextPlayer();
				currentView.updateLabelValues();
			}
			
			else
			{
				System.out.println("[ERROR] You are not fortifying right now!");
			}
			
			return;
		}
		
		if(gamePhase == RiskPhase.DEPLOY)
		{
			unoccupiedCountries = model.getBoard().getUnoccupiedCountries();
			
			// when all countries has been occupied, players can reinforce their occupied countries
			if(unoccupiedCountries.isEmpty())
			{
				model.reinforce(actionCommand, RiskModel.REINFORCE_TERRITORY); // forces value of 0 means receive input from user in 'reinforce' method
			}
			
			// as long as there are unoccupied countries, players will have to occupy them
			else if(!unoccupiedCountries.isEmpty() && !currentCountry.isOccupied())
			{
				model.reinforce(actionCommand, RiskModel.OCCUPY_TERRITORY);
			}
		}
		
		else if(gamePhase == RiskPhase.ATTACK)
		{
			attackCountries.add(actionCommand);
			
			if(attackCountries.size() == RiskModel.COUNTRIES_FOR_ATTACK)
			{
				model.attack(attackCountries.get(0), attackCountries.get(1));
				attackCountries.clear();
			}
		}
		
		else if(gamePhase == RiskPhase.FORTIFY)
		{
			fortifyCountries.add(actionCommand);
			
			if(fortifyCountries.size() == RiskModel.COUNTRIES_FOR_FORTIFY)
			{
				model.fortify(fortifyCountries.get(0), fortifyCountries.get(1));
				fortifyCountries.clear();
			}
		}
		
		//currentView.updateLabelValues();
	}
}

class BoardWindowController extends WindowAdapter
{
	private RiskModel model;
	
	public BoardWindowController(RiskModel model)
	{
		this.model = model;
	}
	
	@Override
	public void windowClosing(WindowEvent windowEvent) // detects a click on window close button
	{ 
		model.quitGame();
	}
}

class BackgroundPanelController implements MouseListener
{
	@Override
	public void mouseClicked(MouseEvent e) {
		//System.out.println("[COORDS] " + e.getX() + ", " + e.getY());
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}

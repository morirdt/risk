package enums;

public enum RiskPhase 
{
	DEPLOY("Deploy"),
	ATTACK("Attack"),
	FORTIFY("Fortify");
	
	private String phase;

	RiskPhase(String phase)
	{
		this.phase = phase;
	}
	
	public String toString()
	{
		return this.phase;
	}
}

package enums;

public enum RiskGamemode 
{
	HUMAN_VS_HUMAN,
	HUMAN_VS_COMPUTER;
}

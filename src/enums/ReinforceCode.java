package enums;

public enum ReinforceCode 
{
	COUNTRY_NOT_FOUND, 
	INVALID_COUNTRY, 
	NOT_ENOUGH_FORCES,
	REINFORCE_FAIL,
	REINFORCE_SUCCESS;
}

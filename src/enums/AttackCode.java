package enums;

/**
 * <h1><code>enum AttackCode</code></h1> 
 * <hr>
 * Allows the attack method in Model to return a code specifying the outcome of a specific attack.<br>
 */
public enum AttackCode 
{
	COUNTRY_NOT_FOUND, 
	INVALID_COUNTRY, 
	NOT_ENOUGH_FORCES, 
	SAME_OCCUPANT, 
	COUNTRIES_NOT_BORDERING,
	NO_WINNERS, 
	ATTACK_FAIL,
	ATTACK_SUCCESS;
}

package uicomponents;

import javax.swing.JFrame;

public abstract class RiskFrame extends JFrame
{
	private static final long serialVersionUID = 1L;

	public void showFrame()
	{
		centerFrame();
		setResizable(false);
		setVisible(true);
	}
	
	public void hideFrame()
	{
		setVisible(false);
	}
	
	/**
     * <em><h1>centerFrame</h1></em>
     * Sets the frame properties to appear at the center of the screen.
     * <hr>
     */
	public void centerFrame()
	{
		setLocationRelativeTo(null); 
	}
	
	/**
     * <em><h1>fullscreenFrame</h1></em>
     * Sets the frame properties to be full screen.
     * <hr>
     */
	public void fullscreenFrame()
	{
		setResizable(true);
		setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);
	}
}

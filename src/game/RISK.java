package game;

import mvc.*;

public class RISK 
{	
	public static void main(String[] args)
	{
		RiskModel model = RiskModel.getInstance();
		RiskView view = RiskView.getInstance();
		RiskController controller = new RiskController(model, view);
	}
}
package botcomponents;

import java.util.ArrayList;
import java.util.List;

import riskcomponents.Board;
import riskcomponents.Country;
import riskcomponents.RiskUtilities;

public class AttackEvaluator implements Evaluator
{
	public int evaluate(Board board) 
	{
		int evaluation = 0;
		
		List<Country> humanCountries = new ArrayList<Country>();
		List<Country> botCountries = new ArrayList<Country>();
		
		sortCountries(board.getCountriesList(), humanCountries, botCountries);
		
		if(checkWin(botCountries))
		{
			evaluation = Integer.MAX_VALUE;
		}
		
		return evaluation;
	}
	
	private void sortCountries(List<Country> allCountries, List<Country> humanCountries, List<Country> botCountries)
	{
		for(Country c : allCountries)
		{
			if(c.isOccupied())
			{
				if(c.getOccupant() instanceof Bot)
					botCountries.add(c);
				
				else
					humanCountries.add(c);
			}
		}
	}
	
	private boolean checkWin(List<Country> botCountries)
	{
		return botCountries.size() == RiskUtilities.TERRITORIES;
	}
}

package botcomponents;

import riskcomponents.*;

public interface Evaluator 
{
	public int evaluate(Board board);
}

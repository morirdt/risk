package botcomponents;

import java.awt.Color;
import java.util.*;


import enums.RiskPhase;
import mvc.RiskModel;
import riskcomponents.Board;
import riskcomponents.Continent;
import riskcomponents.Country;
import riskcomponents.Player;

public class Bot extends Player
{
	private RiskModel model = RiskModel.getInstance();
	
	private List<RiskMove> possibleMoves = new ArrayList<RiskMove>();;
	private AttackEvaluator attackEvaluator = new AttackEvaluator();
	
	public Bot(String _name, int _forces, Color _color) 
	{
		super(_name, _forces, _color);
	}
	
	/**
	 * <em><h1>executeMove</h1></em>
	 * Executes the move for the Bot player. Handles all 3 phases of the Risk game.
	 * <hr>
	 */
	public void executeMove()
	{
		RiskPhase gamePhase = model.getGamePhase();
		
		switch(gamePhase)
		{
			case DEPLOY:
				
				ReinforcementMove reinforcement;
				
				if(!model.getBoard().getUnoccupiedCountries().isEmpty())
				{
					reinforcement = chooseTerritory();
				}
				
				else
				{
					reinforcement = reinforceCountry();
				}
				
				model.reinforce(reinforcement.getTarget().getName(), reinforcement.getForces());
				break;
		
			case ATTACK:

				getPossibleAttackMoves();
				
				if(!possibleMoves.isEmpty())
				{
					AttackMove attack = (AttackMove) chooseBest(possibleMoves, attackEvaluator);			
					model.attack(attack.getAttackerCountry().getName(), attack.getDefenderCountry().getName());
				}
				
				model.incrementPhase();
		
			case FORTIFY:
		
				getPossibleFortificationMoves();
				
				if(possibleMoves.size() > 0)
				{ 
					FortificationMove fortification = (FortificationMove) possibleMoves.get(0);
					model.fortify(fortification.getSourceCountry().getName(), fortification.getDestinationCountry().getName());
				}
			
				model.nextPlayer();
				break;
		}
	}
	
	public int getBotDices(List<Integer> optionsList)
	{
		return optionsList.get(optionsList.size() - 1);
	}
	
	public int getBotFortification(int maxForces)
	{
		return maxForces * 2 / 3;
	}
	
	private List<RiskMove> getPossibleReinforcementMoves()
	{
		possibleMoves.clear();
		List<Country> unoccupiedCountries = model.getBoard().getUnoccupiedCountries();
		
		if(!unoccupiedCountries.isEmpty())
		{
			for(Country c : unoccupiedCountries)
			{
				possibleMoves.add(new ReinforcementMove(c, 1));
			}
		}
		
		else
		{
			for(Country c : occupiedCountries.values())
			{
				possibleMoves.add(new ReinforcementMove(c, forces / 3));
			}
		}
		
		//Collections.sort(possibleMoves);
		return possibleMoves;
	}
	
	private List<RiskMove> getPossibleAttackMoves()
	{
		possibleMoves.clear();
		
		for(Country attackerCountry : occupiedCountries.values()) 
		{
			if(attackerCountry.getForces() > 1)
			{
				for(Country borderingCountry : attackerCountry.getBorderingCountries())
				{
					if(!borderingCountry.getOccupant().equals(this))
					{
						possibleMoves.add(new AttackMove(attackerCountry, borderingCountry));
					}
				}
			}
		}
		
		return possibleMoves;
	}
	
	private List<RiskMove> getPossibleFortificationMoves()
	{
		possibleMoves.clear();
		
		for(Country sourceCountry : occupiedCountries.values())
		{
			if(sourceCountry.getForces() > 1)
			{
				for(Country borderingCountry : sourceCountry.getBorderingCountries())
				{
					if(borderingCountry.getOccupant().equals(this))
					{
						possibleMoves.add(new FortificationMove(sourceCountry, borderingCountry, 1));
					}
				}
			}
		}
		
		return possibleMoves;
	}
	
	private boolean checkCountryInContinent(Continent continent)
	{
		for(Country country : continent.getCountries())
		{
			if(country.isOccupied() && country.getOccupant().equals(this))
			{
				return true;
			}
		}
		
		return false;
	}
	
	private ReinforcementMove getCountryInContinent(Continent continent)
	{
		for(Country c : continent.getCountries())
		{
			if(!c.isOccupied())
			{
				return new ReinforcementMove(c, 1);
			}
		}
		
		return null;
	}
	
	private ReinforcementMove checkContinent(Continent continent)
	{
		ReinforcementMove move = null;
		
		if(!checkCountryInContinent(continent))
		{
			move = getCountryInContinent(continent);
		}
		
		return move;
	}
	
	private ReinforcementMove chooseContinentsInOrder()
	{
		Board board = model.getBoard();
		ReinforcementMove move = null;
		
		if( (move = checkContinent(board.getContinents().get("Australia"))) != null)
		{
			return move;
		}
		
		if( (move = checkContinent(board.getContinents().get("South America"))) != null)
		{
			return move;
		}
		
		if( (move = checkContinent(board.getContinents().get("Africa"))) != null)
		{
			return move;
		}
		
		if( (move = checkContinent(board.getContinents().get("Europe"))) != null)
		{
			return move;
		}
		
		if( (move = checkContinent(board.getContinents().get("North America"))) != null)
		{
			return move;
		}
		
		if( (move = checkContinent(board.getContinents().get("Asia"))) != null)
		{
			return move;
		}
		
		return move;
	}
	
	private ReinforcementMove occupyContinent(Continent continent)
	{
		for(Country c : continent.getCountries())
		{
			if(!c.isOccupied())
			{
				return new ReinforcementMove(c, 1);
			}
		}
		
		return null;
	}
	
	private ReinforcementMove findNeighborInContinent(Country source, Continent continent)
	{
		for(Country c : continent.getCountries())
		{
			if(!c.isOccupied() && c.getBorderingCountries().contains(source))
			{
				return new ReinforcementMove(c, 1);
			}
		}
		
		return null;
	}
	
	private ReinforcementMove chooseTerritory()
	{
		Map<String, Continent> continentsMap = model.getBoard().getContinents();
		ReinforcementMove move;
		Country toCheck;
		
		// occupying one country on each continent
		
		toCheck = model.getBoard().getCountryByName("Indonesia");
		
		if(!toCheck.isOccupied())
		{
			return new ReinforcementMove(toCheck, 1);
		}
		
		if(!checkCountryInContinent(continentsMap.get("Australia")))
		{
			if( (move = findNeighborInContinent(toCheck, continentsMap.get("Australia"))) != null ) 
			{
				return move;
			}
		}
		
		if( (move = chooseContinentsInOrder() ) != null )
		{
			return move;
		}
		
		if( (move = occupyContinent(continentsMap.get("Australia"))) != null )
		{
			return move;
		}
		
		if( (move = occupyContinent(continentsMap.get("Africa"))) != null )
		{
			return move;
		}
		
		if( (move = occupyContinent(continentsMap.get("South America"))) != null )
		{
			return move;
		}
		
		if( (move = occupyContinent(continentsMap.get("North America"))) != null )
		{
			return move;
		}
		
		if( (move = occupyContinent(continentsMap.get("Europe"))) != null )
		{
			return move;
		}
		
		if( (move = occupyContinent(continentsMap.get("Asia"))) != null )
		{
			return move;
		}
		
		return null;
	}
	
	private ReinforcementMove getOwnedNeighbor(Country country, Continent continent)
	{
		for(Country c : continent.getCountries())
		{
			if(occupiedCountries.get(c.getName()) != null && country.getBorderingCountries().contains(c))
			{
				return new ReinforcementMove(c, this.forces);
			}
		}
		
		return null;
	}
	
	private ReinforcementMove reinforceCountry()
	{
		Board board = model.getBoard();
		Country indonesia = board.getCountryByName("Indonesia");
		ReinforcementMove move;
		
		if(occupiedCountries.get(indonesia.getName()) != null)
		{
			return new ReinforcementMove(indonesia, this.forces);
		}
		
		else if( (move = getOwnedNeighbor(indonesia, board.getContinents().get("Australia"))) != null)
		{
			return move;
		}
		
		return null;
	}
	
	private RiskMove chooseBest(List<RiskMove> possibleMoves, Evaluator evaluator)
	{
		int bestEvalutaion = Integer.MIN_VALUE;
		int equalEvaluations = 0;
		
		RiskMove bestMove = possibleMoves.get(0);
		Random rnd = new Random();
		
		for(RiskMove move : possibleMoves)
		{
			Board simulatedBoard = model.simulate(move);
			int actualEvaluation = evaluator.evaluate(simulatedBoard);
			
			if(actualEvaluation > bestEvalutaion)
			{
				bestMove = move;
				bestEvalutaion = actualEvaluation;
				equalEvaluations = 1;
			}
			
			if(actualEvaluation == bestEvalutaion)
			{
				equalEvaluations++;
				int randomNumber = rnd.nextInt();
				
				if(randomNumber * equalEvaluations < 1)
				{
					bestMove = move;
					bestEvalutaion = actualEvaluation;
					equalEvaluations = 1;
				}
			}
		}
		
		return bestMove;
	}
}

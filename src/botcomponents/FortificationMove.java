package botcomponents;

import riskcomponents.Country;

public class FortificationMove implements Comparable<FortificationMove>, RiskMove
{
	private Country sourceCountry;
	private Country destinationCountry;
	private int forces;
	
	public FortificationMove(Country sourceCountry, Country destinationCountry, int forces) 
	{
		this.sourceCountry = sourceCountry;
		this.destinationCountry = destinationCountry;
		this.forces = forces;
	}

	public Country getSourceCountry() 
	{
		return sourceCountry;
	}

	public Country getDestinationCountry() 
	{
		return destinationCountry;
	}

	public int compareTo(FortificationMove o) 
	{
		return sourceCountry.getForces() - o.getSourceCountry().getForces();
	}
}

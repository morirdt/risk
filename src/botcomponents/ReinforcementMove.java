package botcomponents;

import riskcomponents.Country;

public class ReinforcementMove implements Comparable<ReinforcementMove>, RiskMove
{
	private Country target;
	private int forces;
	
	public ReinforcementMove(Country target, int forces)
	{
		this.target = target;
		this.forces = forces;
	}
	
	public Country getTarget()
	{
		return target;
	}
	
	public int getForces()
	{
		return forces;
	}

	public void setForces(int forces)
	{
		this.forces = forces;
	}
	
	public int compareTo(ReinforcementMove o) 
	{
		return this.forces - o.forces;
	}
}

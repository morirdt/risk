package botcomponents;

import riskcomponents.Country;

public class AttackMove implements Comparable<AttackMove>, RiskMove
{
	private Country attackerCountry;
	private Country defenderCountry;
	
	public AttackMove(Country attackerCountry, Country defenderCountry) 
	{
		this.attackerCountry = attackerCountry;
		this.defenderCountry = defenderCountry;
	}

	public Country getAttackerCountry() 
	{
		return attackerCountry;
	}

	public Country getDefenderCountry() 
	{
		return defenderCountry;
	}

	@Override
	public int compareTo(AttackMove o) {
		return this.attackerCountry.getForces() - o.attackerCountry.getForces();
	}
}
